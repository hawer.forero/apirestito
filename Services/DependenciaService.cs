using ApiRest.Models;
using ApiRest.Repositories;
using ApiRest.Repositories.Interfaces;

namespace ApiRest.Services
{
    public class DependenciaService : GenericService<Dependencia>, IDependenciaService
    {
        private IDependenciaRepository dependenciaRepository;

        public DependenciaService(IDependenciaRepository dependenciaRepository) : base(dependenciaRepository)
        {
            this.dependenciaRepository = dependenciaRepository;
        }
    }
}