using ApiRest.Repositories.Interfaces;
using ApiRest.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiRest.Services.Interfaces;


namespace ApiRest.Services
{
    public class GenericService<T> : IGenericService<T> where T : class
    {
        private IGenericRepository<T> genericRepository;

        public GenericService(IGenericRepository<T> genericRepository)
        {
            this.genericRepository = genericRepository;
        }

        public void Delete(long id)
        {
             genericRepository.Delete(id);
        }

        public List<T> GetAll()
        {
            return genericRepository.GetAll();
        }

        public T GetById(long id)
        {
            return  genericRepository.GetById(id);
        }

        public T Insert(T entity)
        {
            return  genericRepository.Insert(entity);
        }

        public T Update(T entity)
        {
            return genericRepository.Update(entity);
        }
    }
}