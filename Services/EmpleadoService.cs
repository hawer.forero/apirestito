using ApiRest.Models;
using ApiRest.Repositories;
using ApiRest.Repositories.Interfaces;

namespace ApiRest.Services
{
    public class EmpleadoService : GenericService<Empleado>, IEmpleadoService
    {
        private IEmpleadoRepository empleadoRepository;

        public EmpleadoService(IEmpleadoRepository empleadoRepository) : base(empleadoRepository)
        {
            this.empleadoRepository = empleadoRepository;
        }
    }
}