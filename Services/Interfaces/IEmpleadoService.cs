using ApiRest.Models;
using ApiRest.Services.Interfaces;
namespace ApiRest.Services
{
    public interface IEmpleadoService : IGenericService<Empleado>
    {
    }
}