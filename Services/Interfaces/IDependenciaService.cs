using ApiRest.Models;
using ApiRest.Services.Interfaces;
namespace ApiRest.Services
{
    public interface IDependenciaService : IGenericService<Dependencia>
    {
    }
}