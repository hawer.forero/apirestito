using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRest.Models
{
    public class Dependencia
    {
        public long Id { get; set;}
        public string Nombre { get; set; }
        public string Sigla { get; set; }
    }
}
