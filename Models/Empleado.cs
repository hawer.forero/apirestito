using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRest.Models
{
    public class Empleado
    {
        public long Id { get; set;}
        public string Nombre { get; set; }
        public long DependenciaId { get; set; }

        public Dependencia Dependencia { get; set; }
    }
}
