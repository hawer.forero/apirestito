using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiRest.Services.Interfaces;
using ApiRest.Services;
using ApiRest.Models;


namespace ApiRest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DependenciaController : ControllerBase
    {
       
        private readonly IDependenciaService _dependenciaService;

        public DependenciaController(IDependenciaService dependenciaService)
        {
            _dependenciaService = dependenciaService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var result = _dependenciaService.GetAll();
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            try
            {
                var result = _dependenciaService.GetById(id);
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Post(Dependencia dependencia)
        {
           try
            {
                var result = _dependenciaService.Insert(dependencia);
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(Dependencia dependencia)
        {
           try
            {
                var result = _dependenciaService.Update(dependencia);
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

         [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                _dependenciaService.Delete(id);
                
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
