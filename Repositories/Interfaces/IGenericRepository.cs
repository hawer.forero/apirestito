using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiRest.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> GetAll();

        T GetById(long id);

        T Insert(T entity);

        T Update(T entity);

        void Delete(long id);
    }
}