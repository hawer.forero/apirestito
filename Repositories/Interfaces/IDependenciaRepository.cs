using ApiRest.Models;

namespace ApiRest.Repositories.Interfaces
{
    public interface IDependenciaRepository : IGenericRepository<Dependencia>
    {
    }
}