using ApiRest.Models;

namespace ApiRest.Repositories.Interfaces
{
    public interface IEmpleadoRepository : IGenericRepository<Empleado>
    {
    }
}