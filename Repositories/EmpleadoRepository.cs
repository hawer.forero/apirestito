using ApiRest.Models;
using ApiRest.Data;
using ApiRest.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRest.Repositories
{
    public class EmpleadoRepository : GenericRepository<Empleado>, IEmpleadoRepository
    {
        private readonly ApplicationDbContext applicationDbContext;

        public EmpleadoRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext) { 
             this.applicationDbContext = applicationDbContext;
        }

        public new  List<Empleado> GetAll()
        {
            var listEmpleados =  applicationDbContext.Empleado.Include(x=> x.Dependencia).ToList();

            return listEmpleados;
        }
    }
}