using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRest.Repositories.Interfaces;
using ApiRest.Data;

namespace ApiRest.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly ApplicationDbContext applicationDbContext;

        public GenericRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public int Count()
        {
            return applicationDbContext.Set<T>().ToList().Count();
        }

        public void Delete(long id)
        {
            var entity = GetById(id);

            if (entity == null)
                throw new Exception("The entity is null");

            applicationDbContext.Set<T>().Remove(entity);
            applicationDbContext.SaveChanges();
        }
       
        public List<T> GetAll()
        {
            try
            {
            return  applicationDbContext.Set<T>().ToList();

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public  T GetById(long id)
        {
            return  applicationDbContext.Set<T>().Find(id);
        }

        public T Insert(T entity)
        {
            applicationDbContext.Set<T>().Add(entity);
            applicationDbContext.SaveChanges();
            return entity;
        }

        public T Update(T entity)
        {
            applicationDbContext.Set<T>().Update(entity);
            applicationDbContext.SaveChanges();
            return entity;
        }
    }
}