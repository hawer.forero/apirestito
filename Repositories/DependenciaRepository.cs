using ApiRest.Models;
using ApiRest.Data;
using ApiRest.Repositories.Interfaces;


namespace ApiRest.Repositories
{
    public class DependenciaRepository : GenericRepository<Dependencia>, IDependenciaRepository
    {
        public DependenciaRepository(ApplicationDbContext dbContext) : base(dbContext) { }
    }
}